-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 13, 2018 at 04:46 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `id5952083_0skr_coffee`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `id_cafe` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `id_cafe`, `nama`, `username`, `password`) VALUES
(1, 1, 'Admin Canai Mamak', 'admin_rawa', 'dc4f755cfcef073db1024ccb32db2c79'),
(2, 2, 'Admin Pizza Huts', 'admin_rawautama', '960c22587f4af9789d8bc5b356dfa556'),
(3, 3, 'Admin Lemore', 'admin_sportivo', '1128329e4417b97ca7efdd04374c9588');

-- --------------------------------------------------------

--
-- Table structure for table `cafe`
--

CREATE TABLE `cafe` (
  `id` int(11) NOT NULL,
  `nama` varchar(40) NOT NULL,
  `alamat` text,
  `jam_buka` time(6) DEFAULT NULL,
  `jam_tutup` time(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `cafe`
--

INSERT INTO `cafe` (`id`, `nama`, `alamat`, `jam_buka`, `jam_tutup`) VALUES
(1, 'Canai Mamak', 'Seutui Banda Aceh', '07:00:00.000000', '23:00:00.000000'),
(2, 'Pizza Huts', 'Simpang 5 Banda Aceh', '07:00:00.000000', '23:00:00.000000'),
(3, 'Lemore', 'Lampineung Banda Aceh', '07:00:00.000000', '23:00:00.000000');

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

CREATE TABLE `meja` (
  `id` int(11) NOT NULL,
  `id_cafe` int(11) NOT NULL,
  `no_meja` varchar(10) NOT NULL,
  `qr_code` varchar(50) NOT NULL,
  `image_url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `meja`
--

INSERT INTO `meja` (`id`, `id_cafe`, `no_meja`, `qr_code`, `image_url`) VALUES
(5, 1, '1', '1_1_rawasakti', '1_1_rawasakti.png'),
(6, 1, '2', '1_2_rawasakti', '1_2_rawasakti.png'),
(7, 1, '3', '1_3_rawasakti', '1_3_rawasakti.png'),
(8, 3, '1A', '3_1A_sportivo', '3_1A_sportivo.png'),
(10, 3, '2B', '3_2B_sportivo', '3_2B_sportivo.png'),
(11, 3, '3C', '3_3C_sportivo', '3_3C_sportivo.png'),
(12, 2, 'A', '2_A_rawautama', '2_A_rawautama.png'),
(13, 2, 'B', '2_B_rawautama', '2_B_rawautama.png'),
(14, 2, 'C', '2_C_rawautama', '2_C_rawautama.png'),
(15, 3, '4D', '3_4D_lemore', '3_4D_lemore.png'),
(16, 1, '4', '1_4_canaimamak', '1_4_canaimamak.png'),
(17, 1, '5', '1_5_canaimamak', '1_5_canaimamak.png');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `id_cafe` int(11) NOT NULL,
  `menu` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL DEFAULT '0',
  `stok` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `id_cafe`, `menu`, `harga`, `stok`) VALUES
(1, 1, 'ROti Bom + Keju', 13000, 200),
(8, 1, 'Lemon tea panas', 12000, 150),
(10, 1, 'Teh tarik Kecil', 10000, 130),
(11, 3, 'Espresso Gayo Arabica', 13000, 200),
(12, 3, 'Black Coffe Arabica', 20000, 150),
(13, 3, 'Sanger Blend', 25000, 100),
(14, 1, 'Nasi goreng', 10000, 150),
(16, 1, 'Cappuchino', 10000, 200),
(17, 3, 'Mie Aceh ', 10000, 200),
(18, 2, 'Spaghety Bolognese', 25000, 200),
(19, 2, 'Green tea blend', 19000, 200),
(20, 2, 'Black Coffe Arabica', 12000, 150),
(21, 1, 'Planta Coklat', 10000, 200),
(22, 1, 'Mie Aceh', 10000, 100),
(23, 1, 'Roti Bom', 12000, 200),
(24, 3, 'Kentang goreng', 15000, 200),
(25, 3, 'Ayam Penyet', 18000, 200);

-- --------------------------------------------------------

--
-- Table structure for table `pesanan`
--

CREATE TABLE `pesanan` (
  `id` int(11) NOT NULL,
  `id_cafe` int(11) NOT NULL,
  `id_meja` int(11) NOT NULL,
  `no_pesanan` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `diskon` int(11) DEFAULT '0',
  `total_harga` int(11) NOT NULL,
  `status` tinyint(5) NOT NULL,
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `pesanan`
--

INSERT INTO `pesanan` (`id`, `id_cafe`, `id_meja`, `no_pesanan`, `tanggal`, `diskon`, `total_harga`, `status`, `updated`) VALUES
(11, 1, 6, '1111', '2018-06-06 02:59:36', 0, 16000, 2, '2018-06-06 16:07:19'),
(12, 2, 12, '1111', '2018-06-06 03:56:48', 0, 64000, 2, '2018-06-06 16:11:20'),
(13, 2, 13, '1111', '2018-06-06 04:07:48', 0, 18000, 2, '2018-06-07 02:32:16'),
(18, 1, 7, '1111', '2018-07-07 06:58:41', 0, 13000, 2, '2018-07-07 19:41:46'),
(20, 1, 5, '1111', '2018-07-07 08:09:35', 0, 9000, 2, '2018-07-07 20:20:43'),
(21, 1, 7, '1111', '2018-07-07 09:00:08', 0, 19000, 2, '2018-07-07 21:03:11'),
(22, 1, 7, '1111', '2018-07-07 09:04:47', 0, 18000, 2, '2018-07-22 14:29:11'),
(23, 1, 5, '1111', '2018-07-16 03:36:41', 0, 13000, 2, '2018-07-16 03:40:36'),
(24, 1, 7, '1111', '2018-07-16 03:41:04', 0, 17000, 2, '2018-07-16 03:48:19'),
(25, 1, 6, '1111', '2018-07-16 03:50:09', 0, 18000, 2, '2018-07-17 03:09:56'),
(26, 1, 5, '1111', '2018-07-17 03:10:40', 0, 26000, 2, '2018-07-17 03:16:17'),
(27, 1, 5, '1111', '2018-07-24 01:41:17', 0, 8000, 2, '2018-07-24 13:43:26'),
(28, 1, 7, '1111', '2018-07-24 01:45:39', 0, 3000, 2, '2018-07-24 14:00:13'),
(29, 1, 5, '1111', '2018-07-24 02:00:51', 0, 21000, 2, '2018-07-30 23:26:18'),
(30, 1, 5, '1111', '2018-07-30 11:27:58', 0, 23000, 2, '2018-08-05 10:14:35'),
(31, 1, 5, '1111', '2018-08-05 10:14:00', 0, 32000, 2, '2018-08-05 10:20:47'),
(32, 1, 5, '1111', '2018-08-05 10:15:53', 0, 34000, 2, '2018-08-05 10:24:32'),
(33, 1, 5, '1111', '2018-08-05 10:23:09', 0, 24000, 2, '2018-08-05 10:32:29'),
(34, 1, 5, '1111', '2018-08-05 10:25:53', 0, 43000, 2, '2018-08-05 10:31:20'),
(35, 1, 5, '1111', '2018-08-05 10:32:10', 0, 35000, 2, '2018-08-05 10:34:08'),
(36, 1, 5, '1111', '2018-08-05 10:33:23', 0, 35000, 2, '2018-08-05 10:37:59'),
(37, 1, 5, '1111', '2018-08-05 10:36:14', 0, 53000, 2, '2018-08-05 10:40:32'),
(38, 1, 5, '1111', '2018-08-05 10:39:32', 0, 45000, 2, '2018-08-05 10:42:00'),
(39, 1, 5, '1111', '2018-08-05 10:41:27', 0, 45000, 2, '2018-08-05 10:44:14'),
(40, 1, 5, '1111', '2018-08-05 10:43:21', 0, 73000, 2, '2018-08-05 11:42:45'),
(41, 1, 5, '1111', '2018-08-05 11:02:58', 0, 23000, 2, '2018-08-05 11:38:22'),
(42, 1, 5, '1111', '2018-08-05 11:08:05', 0, 23000, 2, '2018-08-05 11:41:02'),
(43, 2, 12, '1111', '2018-08-05 11:32:58', 0, 56000, 2, '2018-08-05 11:34:02'),
(44, 1, 5, '1111', '2018-08-05 11:35:06', 0, 35000, 2, '2018-08-05 11:39:32'),
(45, 1, 5, '1111', '2018-08-05 11:39:19', 0, 25000, 2, '2018-08-05 11:50:59'),
(46, 1, 5, '1111', '2018-08-05 11:42:22', 0, 12000, 2, '2018-08-05 11:53:14'),
(47, 1, 6, '1111', '2018-08-05 11:43:44', 0, 13000, 2, '2018-08-05 11:44:46'),
(48, 1, 6, '1111', '2018-08-05 11:45:30', 0, 30000, 2, '2018-08-05 11:47:43'),
(49, 1, 5, '1111', '2018-08-05 11:55:17', 0, 43000, 2, '2018-08-05 11:56:56'),
(50, 1, 5, '1111', '2018-08-15 11:35:26', 0, 25000, 2, '2018-08-15 23:37:39'),
(51, 2, 12, '1111', '2018-08-15 11:41:55', 0, 69000, 2, '2018-08-15 23:43:11'),
(52, 1, 5, '1111', '2018-08-16 02:54:48', 0, 244000, 1, '2018-08-16 03:03:05');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan_detail`
--

CREATE TABLE `pesanan_detail` (
  `id` int(11) NOT NULL,
  `id_pesanan` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `diskon` int(11) DEFAULT '0',
  `sub_harga` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `pesanan_detail`
--

INSERT INTO `pesanan_detail` (`id`, `id_pesanan`, `id_menu`, `qty`, `diskon`, `sub_harga`, `status`) VALUES
(11, 11, 1, 3, 0, 9000, 1),
(12, 11, 10, 1, 0, 7000, 1),
(13, 12, 18, 2, 0, 16000, 1),
(14, 12, 19, 4, 0, 28000, 1),
(15, 13, 20, 3, 0, 18000, 1),
(16, 12, 19, 2, 0, 14000, 1),
(17, 12, 20, 1, 0, 6000, 1),
(31, 18, 8, 2, 0, 10000, 1),
(33, 18, 1, 1, 0, 3000, 1),
(40, 20, 1, 3, 0, 9000, 1),
(44, 21, 10, 2, 0, 14000, 1),
(45, 21, 8, 1, 0, 5000, 1),
(46, 22, 1, 2, 0, 6000, 1),
(47, 22, 8, 1, 0, 5000, 1),
(48, 22, 10, 1, 0, 7000, 1),
(49, 23, 1, 1, 0, 3000, 1),
(50, 23, 8, 2, 0, 10000, 1),
(52, 24, 8, 2, 0, 10000, 1),
(53, 24, 10, 1, 0, 7000, 1),
(54, 25, 1, 1, 0, 3000, 1),
(55, 25, 8, 1, 0, 5000, 1),
(57, 25, 14, 1, 0, 10000, 1),
(59, 26, 16, 2, 0, 16000, 1),
(60, 26, 14, 1, 0, 10000, 1),
(61, 27, 1, 1, 0, 3000, 1),
(62, 27, 8, 1, 0, 5000, 1),
(65, 28, 1, 1, 0, 3000, 0),
(66, 29, 16, 1, 0, 8000, 1),
(67, 29, 14, 1, 0, 10000, 1),
(68, 29, 1, 1, 0, 3000, 1),
(69, 30, 1, 1, 0, 13000, 1),
(70, 30, 16, 1, 0, 10000, 1),
(71, 31, 8, 1, 0, 12000, 1),
(72, 31, 14, 1, 0, 10000, 1),
(73, 31, 22, 1, 0, 10000, 1),
(74, 32, 23, 1, 0, 12000, 1),
(75, 32, 8, 1, 0, 12000, 1),
(76, 32, 10, 1, 0, 10000, 1),
(77, 33, 8, 1, 0, 12000, 1),
(79, 33, 23, 1, 0, 12000, 1),
(80, 34, 1, 1, 0, 13000, 1),
(81, 34, 10, 2, 0, 20000, 1),
(82, 34, 16, 1, 0, 10000, 1),
(83, 35, 1, 1, 0, 13000, 1),
(84, 35, 8, 1, 0, 12000, 1),
(85, 35, 10, 1, 0, 10000, 1),
(86, 36, 1, 1, 0, 13000, 1),
(87, 36, 8, 1, 0, 12000, 1),
(88, 36, 10, 1, 0, 10000, 1),
(89, 37, 10, 1, 0, 10000, 1),
(90, 37, 1, 1, 0, 13000, 1),
(91, 37, 16, 1, 0, 10000, 1),
(92, 37, 22, 2, 0, 20000, 1),
(93, 38, 1, 1, 0, 13000, 1),
(94, 38, 8, 1, 0, 12000, 1),
(95, 38, 14, 2, 0, 20000, 1),
(96, 39, 1, 1, 0, 13000, 1),
(97, 39, 8, 1, 0, 12000, 1),
(98, 39, 14, 2, 0, 20000, 1),
(99, 40, 1, 1, 0, 13000, 1),
(100, 40, 10, 1, 0, 10000, 1),
(101, 40, 22, 2, 0, 20000, 1),
(102, 40, 21, 3, 0, 30000, 1),
(103, 41, 1, 1, 0, 13000, 1),
(104, 41, 16, 1, 0, 10000, 1),
(105, 42, 1, 1, 0, 13000, 1),
(106, 42, 16, 1, 0, 10000, 1),
(107, 43, 18, 1, 0, 25000, 1),
(108, 43, 19, 1, 0, 19000, 1),
(109, 43, 20, 1, 0, 12000, 1),
(110, 44, 1, 1, 0, 13000, 1),
(111, 44, 8, 1, 0, 12000, 1),
(112, 44, 10, 1, 0, 10000, 1),
(113, 45, 1, 1, 0, 13000, 1),
(114, 45, 8, 1, 0, 12000, 1),
(115, 46, 8, 1, 0, 12000, 1),
(116, 47, 1, 1, 0, 13000, 1),
(118, 48, 16, 1, 0, 10000, 1),
(119, 48, 14, 2, 0, 20000, 1),
(120, 49, 1, 1, 0, 13000, 1),
(121, 49, 10, 1, 0, 10000, 1),
(122, 49, 16, 2, 0, 20000, 1),
(123, 50, 1, 1, 0, 13000, 1),
(124, 50, 8, 1, 0, 12000, 1),
(126, 51, 18, 2, 0, 50000, 1),
(127, 51, 19, 1, 0, 19000, 1),
(129, 52, 8, 2, 0, 24000, 1),
(130, 52, 8, 10, 0, 120000, 1),
(131, 52, 16, 10, 0, 100000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(40) NOT NULL,
  `nomor_hp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(150) NOT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama_lengkap`, `nomor_hp`, `email`, `username`, `password`, `created`, `updated`) VALUES
(1, 'Heri Hakim', '085372996841', 'hhakisetiawan@gmail.com', 'herihakim', '17ebec54ad4f0c3638478ee5b3ff4f42', '2018-05-20 14:05:59', '2018-05-20 14:09:35'),
(3, 'M Ridho', '085362718293', 'ridho@gmail.com', 'ridho', '926a161c6419512d711089538c80ac70', '2018-05-25 18:26:00', NULL),
(4, 'Tes', '083213121312', 'tes@gmail.com', 'tes1234', 'a19ea7dff6ced87d8cb9c62cc7922497', '2018-05-26 10:23:55', NULL),
(5, 'setiawan', '085372461346', 'setiawan@gmail.com', 'setiawan', '41591fa3a697604be431ef66b5f53572', '2018-05-26 23:02:59', NULL),
(6, 'rahmad', '082312584310', 'ragmadhidayat324@gmail.com', 'rahmadhidayat', '0d72f3f3120954245ac23e1338cf2136', '2018-06-06 15:43:32', NULL),
(7, 'Rahmad Hidayat', '082312584310', 'rahmadhidayatti@gmail.com', 'rahmad', 'e10adc3949ba59abbe56e057f20f883e', '2018-07-16 03:15:33', NULL),
(8, 'RahmadHidayat', '085221820242', 'rahmadhidayatti13@gmail.com', 'rahmad', 'e10adc3949ba59abbe56e057f20f883e', '2018-07-24 13:33:29', NULL),
(9, 'Rahmad Hidayat', '082312584310', 'rahmadhidayat324@gmail.com', 'rahmadhidayat', '0d72f3f3120954245ac23e1338cf2136', '2018-08-05 10:03:13', NULL),
(10, 'dayat', '082312584310', 'dayat@gmail.con', 'dayat', '1855c11f044cc8944e8cdac9cae5def8', '2018-08-05 10:35:11', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `relasi_cafe` (`id_cafe`) USING BTREE;

--
-- Indexes for table `cafe`
--
ALTER TABLE `cafe`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `meja`
--
ALTER TABLE `meja`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `meja_cafe` (`id_cafe`) USING BTREE;

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `cafe` (`id_cafe`) USING BTREE;

--
-- Indexes for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `cafe_pesan` (`id_cafe`) USING BTREE,
  ADD KEY `meja_pesan` (`id_meja`) USING BTREE;

--
-- Indexes for table `pesanan_detail`
--
ALTER TABLE `pesanan_detail`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `pesanan_detail` (`id_pesanan`) USING BTREE,
  ADD KEY `pesanan_menu` (`id_menu`) USING BTREE;

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cafe`
--
ALTER TABLE `cafe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `meja`
--
ALTER TABLE `meja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `pesanan_detail`
--
ALTER TABLE `pesanan_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;
