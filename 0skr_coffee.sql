/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50620
 Source Host           : localhost:3306
 Source Schema         : 0skr_coffee

 Target Server Type    : MySQL
 Target Server Version : 50620
 File Encoding         : 65001

 Date: 26/05/2018 23:02:59
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cafe` int(11) NOT NULL,
  `nama` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `relasi_cafe`(`id_cafe`) USING BTREE,
  CONSTRAINT `relasi_cafe` FOREIGN KEY (`id_cafe`) REFERENCES `cafe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 1, 'Admin Rawa Sakti', 'admin_rawa', 'dc4f755cfcef073db1024ccb32db2c79');
INSERT INTO `admin` VALUES (2, 2, 'Admin Rawa Utama', 'admin_rawautama', '960c22587f4af9789d8bc5b356dfa556');
INSERT INTO `admin` VALUES (3, 3, 'Admin Sportivo', 'admin_sportivo', '1128329e4417b97ca7efdd04374c9588');

-- ----------------------------
-- Table structure for cafe
-- ----------------------------
DROP TABLE IF EXISTS `cafe`;
CREATE TABLE `cafe`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `jam_buka` time(6) NULL DEFAULT NULL,
  `jam_tutup` time(6) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cafe
-- ----------------------------
INSERT INTO `cafe` VALUES (1, 'Rawa Sakti', 'Jl Rawa Sakti Banda Aceh', '07:00:00.000000', '23:00:00.000000');
INSERT INTO `cafe` VALUES (2, 'Rawa Utama', 'Jl Rawa Utama Banda Aceh', '07:00:00.000000', '23:00:00.000000');
INSERT INTO `cafe` VALUES (3, 'Sportivo', 'Jl Sportivo Banda Aceh', '07:00:00.000000', '23:00:00.000000');

-- ----------------------------
-- Table structure for meja
-- ----------------------------
DROP TABLE IF EXISTS `meja`;
CREATE TABLE `meja`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cafe` int(11) NOT NULL,
  `no_meja` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `qr_code` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `image_url` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `meja_cafe`(`id_cafe`) USING BTREE,
  CONSTRAINT `meja_cafe` FOREIGN KEY (`id_cafe`) REFERENCES `cafe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of meja
-- ----------------------------
INSERT INTO `meja` VALUES (5, 1, '1', '1_1_rawasakti', '1_1_rawasakti.png');
INSERT INTO `meja` VALUES (6, 1, '2', '1_2_rawasakti', '1_2_rawasakti.png');
INSERT INTO `meja` VALUES (7, 1, '3', '1_3_rawasakti', '1_3_rawasakti.png');
INSERT INTO `meja` VALUES (8, 3, '1A', '3_1A_sportivo', '3_1A_sportivo.png');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cafe` int(11) NOT NULL,
  `menu` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `harga` int(11) NOT NULL DEFAULT 0,
  `stok` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cafe`(`id_cafe`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 1, 'Kopi', 3000, 100);
INSERT INTO `menu` VALUES (8, 1, 'Teh Dingin', 5000, 150);
INSERT INTO `menu` VALUES (10, 1, 'Sanger Dingin', 7000, 130);

-- ----------------------------
-- Table structure for pesanan
-- ----------------------------
DROP TABLE IF EXISTS `pesanan`;
CREATE TABLE `pesanan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cafe` int(11) NOT NULL,
  `id_meja` int(11) NOT NULL,
  `no_pesanan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal` datetime(0) NOT NULL,
  `diskon` int(11) NULL DEFAULT 0,
  `total_harga` int(11) NOT NULL,
  `status` tinyint(5) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `cafe_pesan`(`id_cafe`) USING BTREE,
  INDEX `meja_pesan`(`id_meja`) USING BTREE,
  CONSTRAINT `cafe_pesan` FOREIGN KEY (`id_cafe`) REFERENCES `cafe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `meja_pesan` FOREIGN KEY (`id_meja`) REFERENCES `meja` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pesanan
-- ----------------------------
INSERT INTO `pesanan` VALUES (1, 1, 5, '1234', '2018-05-25 18:26:00', 0, 20000, 0);
INSERT INTO `pesanan` VALUES (4, 1, 5, '4321', '2018-05-25 06:26:00', 0, 50000, 0);

-- ----------------------------
-- Table structure for pesanan_detail
-- ----------------------------
DROP TABLE IF EXISTS `pesanan_detail`;
CREATE TABLE `pesanan_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pesanan` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `diskon` int(11) NULL DEFAULT 0,
  `sub_harga` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pesanan_detail`(`id_pesanan`) USING BTREE,
  INDEX `pesanan_menu`(`id_menu`) USING BTREE,
  CONSTRAINT `pesanan_detail` FOREIGN KEY (`id_pesanan`) REFERENCES `pesanan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pesanan_menu` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pesanan_detail
-- ----------------------------
INSERT INTO `pesanan_detail` VALUES (2, 1, 10, 5, 0, 50000);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nomor_hp` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `updated` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'Heri Hakim', '085372996841', 'hhakisetiawan@gmail.com', 'herihakim', '17ebec54ad4f0c3638478ee5b3ff4f42', '2018-05-20 14:05:59', '2018-05-20 14:09:35');
INSERT INTO `user` VALUES (3, 'M Ridho', '085362718293', 'ridho@gmail.com', 'ridho', '926a161c6419512d711089538c80ac70', '2018-05-25 18:26:00', NULL);
INSERT INTO `user` VALUES (4, 'Tes', '083213121312', 'tes@gmail.com', 'tes1234', 'a19ea7dff6ced87d8cb9c62cc7922497', '2018-05-26 10:23:55', NULL);

SET FOREIGN_KEY_CHECKS = 1;
