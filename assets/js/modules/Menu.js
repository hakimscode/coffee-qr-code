$(document).ready(function(){

	$('#tambah').click(function() {
		if($('#nama_menu').val() == ""){
			alert('Nama Menu harus diisi');
			$('#nama_menu').focus();
		}else if($('#harga_menu').val() == ""){
			alert('Harga Menu harus diisi');
			$('#harga_menu').focus();
		}else if($('#stok_menu').val() == ""){
			alert('Stok Menu harus diisi');
			$('#stok_menu').focus();
		}else{
			save();	
		}
	});

});

var save_method = "";

function form_add(){
	$('.form')[0].reset();
	$(".modal-title").html("Tambah Menu Baru");
	$('.modal').show();
	$('#nama_menu').focus();
	save_method = 'add';
}

function form_edit(id){
	$(".modal-title").html("Edit Menu");
	$('.modal').show();
	$('#nama_menu').focus();
	save_method = 'update';

	$.ajax({
		url : site_url + "/menus/display_form_edit/" + id,
		type : "GET",
		dataType : "JSON",
		success : function(data){
			$("#id").val(data.id);
			$("#nama_menu").val(data.menu);
			$("#harga_menu").val(data.harga);
			$("#stok_menu").val(data.stok);
		},
		error : function() {
			alert(errorThrown + " Error");
		}
	});
}

function save(){
	var url = "";

	if(save_method == "add"){
		url = site_url + "/menus/process_save";
	}else if(save_method = "update"){
		url = site_url + "/menus/process_edit";
	}

	$.ajax({
		url : url,
		type : "post",
		data: $('.form').serialize(),
        dataType: "JSON",
		success : function(data){
			$('.modal').modal('hide');
            location.reload();// for reload a page
		},
		error : function(jqXHR, textStatus, errorThrown){
			alert(errorThrown + " Error");
		}
	});
}

function delete_data(id){
	console.log(id);
	var q = confirm('Apakah anda yakin ingin menghapus data ini ?');

	if(q == true){
		var id = id;

		$.ajax({
			url : site_url + "/menus/process_delete",
			type : "post",
			data : {
				id : id
			},
			dataType : "JSON",
			success : function(){
				location.reload();
			},
			error : function(){
				alert(errorThrown + " Error");
			}
		});	
	}
}