$(document).ready(function(){

	$('#tambah').click(function() {
		if($('#no_meja').val() == ""){
			alert('Nomor meja harus diisi');
			$('#no_meja').focus();
		}else{
			save();	
		}
	});

});

var save_method = "";

function form_add(){
	$('.form')[0].reset();
	$(".modal-title").html("Tambah Meja Baru");
	$('.modal').show();
	$('#nama_meja').focus();
	save_method = 'add';
}

function form_edit(id){
	$(".modal-title").html("Edit Meja");
	$('.modal').show();
	$('#nama_meja').focus();
	save_method = 'update';

	$.ajax({
		url : site_url + "/mejas/display_form_edit/" + id,
		type : "GET",
		dataType : "JSON",
		success : function(data){
			$("#id").val(data.id);
			$("#no_meja").val(data.no_meja);
		},
		error : function() {
			alert(errorThrown + " Error");
		}
	});
}

function save(){
	var url = "";

	if(save_method == "add"){
		url = site_url + "/mejas/process_save";
	}else if(save_method = "update"){
		url = site_url + "/mejas/process_edit";
	}

	$.ajax({
		url : url,
		type : "post",
		data: $('.form').serialize(),
        dataType: "JSON",
		success : function(data){
			$('.modal').modal('hide');
            location.reload();// for reload a page
		},
		error : function(jqXHR, textStatus, errorThrown){
			alert(errorThrown + " Error");
		}
	});
}

function delete_data(id){
	console.log(id);
	var q = confirm('Apakah anda yakin ingin menghapus data ini ?');

	if(q == true){
		var id = id;

		$.ajax({
			url : site_url + "/mejas/process_delete",
			type : "post",
			data : {
				id : id
			},
			dataType : "JSON",
			success : function(){
				location.reload();
			},
			error : function(){
				alert(errorThrown + " Error");
			}
		});	
	}
}