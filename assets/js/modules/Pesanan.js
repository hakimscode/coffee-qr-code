$(document).ready(function(){

});

function proses_pesanan(id){
	var id = id;

	$.ajax({
		url : site_url + "/pesanans/proses_pesanan_admin/"+id,
		dataType : "JSON",
		success : function(){
			location.reload();
		},
		error : function(){
			alert(errorThrown + " Error");
		}
	});
}


function finishing(id){
	var id = id;

	$.ajax({
		url : site_url + "/pesanans/cek_pesanan_yang_belum_diproses/"+id,
		dataType : "JSON",
		success : function(data){
			if(data.status){
				var q = confirm('Apakah anda yakin ingin closing pesanan ini ?');
				if(q == true){
					$.ajax({
						url : site_url + "/pesanans/finishing_admin/"+id,
						dataType : "JSON",
						success : function(){
							location.reload();
						},
						error : function(){
							alert(errorThrown + " Error");
						}
					});	
				}
			}else{
				alert('Masih ada pesanan yang belum diproses');
			}
		},
		error : function(){
			alert(errorThrown + " Error");
		}
	});
}