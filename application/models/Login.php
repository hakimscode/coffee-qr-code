<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	/**
	* 
	*/
	class Login extends CI_Model{
		private $id;
		private $username;
		private $password;
		private $nama;
		private $id_cafe;
		private $nama_cafe;
		
		function __construct(){
			parent::__construct();
		}

		function setID($id){ $this->id = $id; }
		function getID(){ return $this->id; }
		function setUsername($username){ $this->username = $username; }
		function getUsername(){ return $this->username; }
		function setPassword($password){ $this->password = $password; }
		function getPassword(){ return $this->password; }
		function setNama($nama){ $this->nama = $nama; }
		function getNama(){ return $this->nama; }
		function setIDCafe($id_cafe){ $this->id_cafe = $id_cafe; }
		function getIDCafe(){ return $this->id_cafe; }
		function setNamaCafe($nama_cafe){ $this->nama_cafe = $nama_cafe; }
		function getNamaCafe(){ return $this->nama_cafe; }

		function process_login_admin(){
			$param = array(
				'username' => $this->getUsername(),
				'password' => $this->getPassword()
			);

			$this->db->select('admin.id, admin.username, admin.nama, cafe.id as idcafe, cafe.nama as namacafe');
			$this->db->from('admin');
			$this->db->join('cafe', 'cafe.id = admin.id_cafe');
			$this->db->where($param);
			$this->db->limit(1);

			$result = $this->db->get();

			if($result->num_rows() > 0){
				foreach ($result->result() as $val) {
					$this->setID($val->id);
					$this->setNama($val->nama);
					$this->setNamaCafe($val->namacafe);
					$this->setIDCafe($val->idcafe);
				}
				return "success";
			}else{
				return "failed";
			}
		}
	}
?>