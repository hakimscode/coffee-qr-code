<?php
	
class Menu extends CI_Model{

	private $id;
	private $id_cafe;
	private $qrcode;
	private $menu;
	private $harga;
	private $stok;

	function __construct(){
		parent::__construct();
	}

	function setID($id){ $this->id = $id; }
	function getID(){ return $this->id; }
	function setIDCafe($id_cafe){ $this->id_cafe = $id_cafe; }
	function getIDCafe(){ return $this->id_cafe; }
	function setQRCode($qrcode){ $this->qrcode = $qrcode; }
	function getQRCode(){ return $this->qrcode; }
	function setMenu($menu){ $this->menu = $menu; }
	function getMenu(){ return $this->menu; }
	function setHarga($harga){ $this->harga = $harga; }
	function getHarga(){ return $this->harga; }
	function setStok($stok){ $this->stok = $stok; }
	function getStok(){ return $this->stok; }

	function list_data_web(){
		$this->db->select('menu.id, menu.menu, menu.harga, menu.stok, cafe.nama as nama_cafe');
		$this->db->from('menu');
		$this->db->join('cafe', 'menu.id_cafe = cafe.id');
		$this->db->where('menu.id_cafe', $this->getIDCafe());

		$query = $this->db->get();
		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return NULL;
		}
	}

	function list_data(){
		$this->db->select('menu.id, menu.menu, menu.harga, menu.stok, cafe.nama as nama_cafe, meja.id as id_meja');
		$this->db->from('menu');
		$this->db->join('cafe', 'menu.id_cafe = cafe.id');
		$this->db->join('meja', 'meja.id_cafe = cafe.id');
		$this->db->where('menu.id_cafe', $this->getIDCafe());
		$this->db->where('meja.qr_code', $this->getQRCode());

		$query = $this->db->get();
		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return NULL;
		}
	}

	function insert_data(){
		$data = array(
			'id_cafe' => $this->getIDCafe(),
			'menu' => $this->getMenu(),
			'harga' => $this->getHarga(),
			'stok' => $this->getStok()
		);
		$this->db->insert('menu', $data);
		return $this->db->insert_id();
	}

	function get_detail(){
		$this->db->select('id, menu, harga, stok');
		$this->db->from('menu');
		$this->db->where('id', $this->getID());

		$query = $this->db->get();
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return FALSE;
		}
	}

	function edit_data(){
		$data = array(
			'menu' => $this->getMenu(),
			'harga' => $this->getHarga(),
			'stok' => $this->getStok()
		);

		$this->db->update('menu', $data, array('id'=>$this->getID()));
		return $this->db->affected_rows();
	}

	function delete_data(){
		$this->db->delete('menu', array('id'=>$this->getID()));
		return $this->db->affected_rows();
	}

}

?>