<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div id="content-header">
	<div id="breadcrumb"> <a href="<?=site_url();?>/menus" title="Go to Home" class="tip-bottom"><i class="icon-signal"></i> Daftar Menu</a></div>
	<h1>Daftar Menu</h1>
</div>


<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">

			<!-- Session Flash Data Pesan Error -->
			<?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success">
                <button class="close" data-dismiss="alert">x</button>
                <strong>Berhasil,</strong> <?=$this->session->flashdata('success');?>
            </div>
            <?php elseif($this->session->flashdata('warning')): ?>
            <div class="alert alert-danger">
                <button class="close" data-dismiss="alert">x</button>
                <strong>Gagal,</strong> <?=$this->session->flashdata('warning');?>
            </div>
            <?php endif; ?>
            <!-- End Session Data Pesan Error -->

			<div class="buttons">
				<a id="add-event" data-toggle="modal" href="#modal-add-event" onclick="form_add()" class="btn btn-primary"><i class="icon-plus icon-white"></i> Tambah Menu</a>
			</div>

			<div class="widget-box">
				<div class="widget-title">
					<span class="icon"><i class="icon-th"></i></span> 
					<h5>Data Menu</h5>
					
				</div>
				<div class="widget-content nopadding">
					<table class="table table-bordered data-table">
						<thead>
							<tr>
								<th width="3%">No</th>
								<th width="60%">Menu</th>
								<th width="15%">Harga</th>
								<th width="10%">Stok</th>
								<th width="12%">Respon</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if($result > 0){
							$nomor = 1;
							foreach ($result as $key => $value) {
							?>
							<tr>
								<td class="center"><?=$nomor;?></td>
								<td><?=$value['menu'];?></td>
								<td class="right"><?=$value['harga'];?></td>
								<td class="center"><?=$value['stok'];?></td>
								<td class="center">
									<a href="#modal-add-event" data-toggle="modal" onclick="form_edit(<?=$value['id']?>)" class="btn-primary btn-warning btn-small" title="Edit"><i class="icon-pencil icon-white"></i></a>
									<a href="#" onclick="delete_data(<?=$value['id'];?>)" class="btn-primary btn-danger btn-small" title="Hapus"><i class="icon-trash icon-white"></i></a>
								</td>
							</tr>
							<?php
							$nomor++;
							}
						}
						?>
						</tbody>
	      			</table>
	      		</div>
	      	</div>
	      </div>
	  </div>
</div>


<div class="modal hide" id="modal-add-event">
	 <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3 class="modal-title"></h3>
	</div>
	<div class="modal-body">
		<form class="form form-horizontal" method="post" name="basic_validate" id="basic_validate" novalidate="novalidate">
			<div class="control-group">
                <label class="control-label">Nama Menu</label>
                <div class="controls">
                	<input type="hidden" name="id" id="id">
                    <input type="text" name="nama_menu" id="nama_menu" required>
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Harga Menu</label>
                <div class="controls">
                    <input type="text" class="input_angka" name="harga_menu" id="harga_menu" required>
                </div>
            </div>
			<div class="control-group">
                <label class="control-label">Stok</label>
                <div class="controls">
                    <input type="text" class="input_angka" name="stok_menu" id="stok_menu" required>
                </div>
            </div>
        </form>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Cancel</a>
		<input type="submit" id="tambah" class="btn btn-primary" value="Simpan"/>
	</div>
</div>


<script type="text/javascript" src="<?=base_url();?>assets/js/modules/Menu.js"></script>