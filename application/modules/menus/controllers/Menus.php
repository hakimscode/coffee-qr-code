<?php
	
class Menus extends MY_Controller{

	function __construct(){
		parent::__construct();
		$this->check_session();
		$this->load->model('menu');
	}

	function index(){
		$id_cafe = $this->session->userdata('id_cafe');
		$this->menu->setIDCafe($id_cafe);
		$data['result'] = $this->menu->list_data_web();
		$this->load->view('layout/header');
		$this->load->view('menu', $data);
		$this->load->view('layout/footer');
	}

	function process_save(){
		$id_cafe = $this->session->userdata('id_cafe');
		$this->menu->setIDCafe($id_cafe);
		$this->menu->setMenu($this->security->xss_clean($this->input->post('nama_menu')));
		$this->menu->setHarga($this->security->xss_clean($this->input->post('harga_menu')));
		$this->menu->setStok($this->security->xss_clean($this->input->post('stok_menu')));

		$insert = $this->menu->insert_data();

		if ($insert) {
			$this->session->set_flashdata('success', 'data berhasil ditambah');
			echo json_encode(array("status" => TRUE));
		}else {
			$this->session->set_flashdata('warning', 'terjadi kesalahan');
			echo json_encode(array("status" => FALSE));
		}
	}

	function display_form_edit(){
		$id = $this->uri->segment(3);

		$this->menu->setID($id);
		$row = $this->menu->get_detail();

		$data = array(
			'id' => $row['id'],
			'menu' => $row['menu'],
			'harga' => $row['harga'],
			'stok' => $row['stok']
		);

		echo json_encode($data);
	}

	function process_edit(){
		$this->menu->setID($this->security->xss_clean($this->input->post('id')));
		$this->menu->setMenu($this->security->xss_clean($this->input->post('nama_menu')));
		$this->menu->setHarga($this->security->xss_clean($this->input->post('harga_menu')));
		$this->menu->setStok($this->security->xss_clean($this->input->post('stok_menu')));

		$edit = $this->menu->edit_data();

		if($edit){
			$this->session->set_flashdata('success', 'data berhasil diedit');
		}else{
			$this->session->set_flashdata('warning', 'terjadi kesalahan');
		}
	}

	function process_delete(){
		$id = $_POST['id'];

		$this->menu->setID($id);
		$delete = $this->menu->delete_data();

		if($delete){
			$this->session->set_flashdata('success', 'data berhasil dihapus');
		}else{
			$this->session->set_flashdata('warning', 'terjadi kesalahan');
		}
	}

}

?>