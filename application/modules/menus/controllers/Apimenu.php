<?php

class Apimenu extends REST_Controller{

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->model('menu');
    }

    //Menampilkan data GET
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $data = $this->db->get('menu')->result();
        } else {
            $this->db->where('id', $id);
            $data = $this->db->get('menu')->result();
        }
        $this->response($data, 200);
    }

    function menu_post(){
        $id_cafe = $this->post('id_cafe');
        $qrcode = $this->post('qrcode');

        $resp['menu'] = array();

        $this->menu->setIDCafe($id_cafe);
        $this->menu->setQRCode($qrcode);
        $data = $this->menu->list_data();
        if($data){
            foreach ($data as $key => $val) {
                $menu = array();
                $menu['id'] = $val['id'];
                $menu['menu'] = $val['menu'];
                $menu['harga'] = $val['harga'];
                $menu['stok'] = $val['stok'];
                array_push($resp['menu'], $menu);
            }
            $resp['nama_cafe'] = $val['nama_cafe'];
            $resp['id_meja'] = $val['id_meja'];
            $resp['success'] = 1;
            $resp['message'] = 'Berhasil';
        }else{
            $resp['success'] = 0;
            $resp['message'] = 'Data Tidak Ada';
        }
        echo json_encode($resp);
    }

    //Mengirim atau menambah data baru POST
	function index_post() {
        $data = array(
            'id_cafe'	=> $this->post('id_cafe'),
            'menu'	=> $this->post('menu'),
            'harga'	=> $this->post('harga'),
            'stok'	=> $this->post('stok')
        );
        $insert = $this->db->insert('menu', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    //Memperbarui data yang sudah ada PUT
    function index_put(){
    	$id = $this->put('id');
    	$data = array(
    		'id_cafe'	=> $this->put('id_cafe'),
    		'menu'		=> $this->put('menu'),
    		'harga'		=> $this->put('harga'),
    		'stok'		=> $this->put('stok')
    	);
    	$this->db->where('id', $id);
    	$update = $this->db->update('menu', $data);
    	if($update){
    		$this->response($data, 200);
    	}else{
    		$this->response(array('status' => 'fail', 502));
    	}
    }

    //Menghapus data DELETE
    function index_delete(){
    	$id = $this->delete('id');
    	$this->db->where('id', $id);
    	$delete = $this->db->delete('menu');
    	if($delete){
    		$this->response(array('status' => 'success', 201));
    	}else{
    		$this->response(array('status' => 'fail', 502));
    	}
    }

}
?>