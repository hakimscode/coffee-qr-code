<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div id="content-header">
	<div id="breadcrumb"> <a href="<?=site_url();?>/Mejas" title="Go to Home" class="tip-bottom"><i class="icon-qrcode"></i> Meja QR Code</a></div>
	<h1>Meja QR Code</h1>
</div>


<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">

			<!-- Session Flash Data Pesan Error -->
			<?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success">
                <button class="close" data-dismiss="alert">x</button>
                <strong>Berhasil,</strong> <?=$this->session->flashdata('success');?>
            </div>
            <?php elseif($this->session->flashdata('warning')): ?>
            <div class="alert alert-danger">
                <button class="close" data-dismiss="alert">x</button>
                <strong>Gagal,</strong> <?=$this->session->flashdata('warning');?>
            </div>
            <?php endif; ?>
            <!-- End Session Data Pesan Error -->

			<div class="buttons">
				<a id="add-event" data-toggle="modal" href="#modal-add-event" onclick="form_add()" class="btn btn-primary"><i class="icon-plus icon-white"></i> Tambah Meja</a>
			</div>

			<div class="widget-box">
				<div class="widget-title">
					<span class="icon"><i class="icon-th"></i></span> 
					<h5>Data Meja</h5>
					
				</div>
				<div class="widget-content nopadding">
					<table class="table table-bordered data-table">
						<thead>
							<tr>
								<th width="3%">No</th>
								<th width="35%">Cafe</th>
								<th width="10%">No Meja</th>
								<th width="30%">QR Code</th>
								<th width="12%">Respon</th>
								<th width="10%">Print</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if($result > 0){
							$nomor = 1;
							foreach ($result as $key => $value) {
							?>
							<tr>
								<td class="center"><?=$nomor;?></td>
								<td><?=$value['namacafe'];?></td>
								<td class="center"><?=$value['nomeja'];?></td>
								<td class="center"><img src="<?=base_url();?>assets/qrcode/<?=$value['imageurl'];?>" width='50px' height='50px'></img></td>
								<td class="center">
									<a href="#modal-add-event" data-toggle="modal" onclick="form_edit(<?=$value['idmeja']?>)" class="btn-primary btn-warning btn-small" title="Edit"><i class="icon-pencil icon-white"></i></a>
									<a href="#" onclick="delete_data(<?=$value['idmeja'];?>)" class="btn-primary btn-danger btn-small" title="Hapus"><i class="icon-trash icon-white"></i></a>
								</td>
								<td class="center">
									<a href="<?=site_url();?>/mejas/print_qrcode/<?=$value['idmeja']?>" target="blank" class="btn-primary btn-success btn-small" title="Print QR Code"><i class="icon-print icon-white"></i></a>
								</td>
							</tr>
							<?php
							$nomor++;
							}
						}
						?>
						</tbody>
	      			</table>
	      		</div>
	      	</div>
	      </div>
	  </div>
</div>


<div class="modal hide" id="modal-add-event">
	 <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3 class="modal-title"></h3>
	</div>
	<div class="modal-body">
		<form class="form form-horizontal" method="post" name="basic_validate" id="basic_validate" novalidate="novalidate">
			<div class="control-group">
                <label class="control-label">No Meja</label>
                <div class="controls">
                	<input type="hidden" name="id" id="id">
                    <input type="text" name="no_meja" id="no_meja" required>
                </div>
            </div>
        </form>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Cancel</a>
		<input type="submit" id="tambah" class="btn btn-primary" value="Simpan"/>
	</div>
</div>


<script type="text/javascript" src="<?=base_url();?>assets/js/modules/Meja.js"></script>