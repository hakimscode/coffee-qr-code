<!DOCTYPE html>
<html>
<head>
	<title>Print QR Code</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap-responsive.min.css" />
	<script type="text/javascript">
		// window.print();
	</script>
</head>
<body>
	<div id="content">
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12" align="center">
					<p><h2><?=$nama_cafe?></h2></p>
					<p><h2><?=$no_meja?></h2></p>
					<img src="<?=base_url();?>assets/qrcode/<?=$image_url?>" width='250px' height='250px'></img>
				</div>
			</div>
		</div>
	</div>
</body>
</html>