<?php
	
class Meja extends CI_Model{

	private $id;
	private $id_cafe;
	private $meja;
	private $qrcode;
	private $imageurl;

	function __construct(){
		parent::__construct();
	}

	function setID($id){ $this->id = $id; }
	function getID(){ return $this->id; }
	function setIDCafe($id_cafe){ $this->id_cafe = $id_cafe; }
	function getIDCafe(){ return $this->id_cafe; }
	function setNoMeja($meja){ $this->meja = $meja; }
	function getNoMeja(){ return $this->meja; }
	function setQRCode($qrcode){ $this->qrcode = $qrcode; }
	function getQRCode(){ return $this->qrcode; }
	function setImageURL($imageurl){ $this->imageurl = $imageurl; }
	function getImageURL(){ return $this->imageurl; }

	function list_data(){
		$this->db->select(
			'meja.id as idmeja, 
			meja.no_meja as nomeja, 
			cafe.nama as namacafe, 
			meja.qr_code as qrcode,
			meja.image_url as imageurl'
		);
		$this->db->from('meja');
		$this->db->join('cafe', 'cafe.id = meja.id_cafe');
		$this->db->where('id_cafe', $this->getIDCafe());

		$query = $this->db->get();
		if($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return FALSE;
		}
	}

	function insert_data(){
		$data = array(
			'id_cafe' => $this->getIDCafe(),
			'no_meja' => $this->getNoMeja(),
			'qr_code' => $this->getQRCode(),
			'image_url' => $this->getImageURL()
		);
		$this->db->insert('meja', $data);
		return $this->db->insert_id();
	}

	function get_detail(){
		$this->db->select('id, no_meja, qr_code, image_url');
		$this->db->from('meja');
		$this->db->where('id', $this->getID());

		$query = $this->db->get();
		if($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return FALSE;
		}
	}

	function edit_data(){
		$data = array(
			'no_meja' => $this->getNoMeja(),
			'qr_code' => $this->getQRCode(),
			'image_url' => $this->getImageURL()
		);

		$this->db->update('meja', $data, array('id'=>$this->getID()));
		return $this->db->affected_rows();
	}

	function delete_data(){
		$this->db->delete('meja', array('id'=>$this->getID()));
		return $this->db->affected_rows();
	}

}

?>