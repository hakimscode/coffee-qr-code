<?php
	
class Mejas extends MY_Controller{

	function __construct(){
		parent::__construct();
		$this->check_session();
		$this->load->model('meja');
	}

	function index(){
		$id_cafe = $this->session->userdata('id_cafe');
		$this->meja->setIDCafe($id_cafe);
		$data['result'] = $this->meja->list_data();
		$this->load->view('layout/header');
		$this->load->view('meja', $data);
		$this->load->view('layout/footer');
	}

	function process_save(){
		$this->load->library('ciqrcode');
		$config['cacheable'] 	= true;
		$config['cachedir'] 	= './assests/';
		$config['errorlog'] 	= './assests/';
		$config['imagedir']		= './assets/qrcode/';
		$config['quality']		= true;
		$config['size']			= '1024';
		$config['black']		= array(224,255,255);
		$config['white']		= array(70,130,180);
		$this->ciqrcode->initialize($config);

		$id_cafe = $this->session->userdata('id_cafe');
		$nama_cafe = $this->session->userdata('nama_cafe');
		$no_meja = $this->input->post('no_meja');

		$rep_nama_cafe = str_replace(' ', '', strtolower($nama_cafe));

		$data_qrcode = $id_cafe . "_" . $no_meja . "_" . $rep_nama_cafe;

		$image_name = $data_qrcode . ".png";

		$params['data'] = $data_qrcode;
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.$config['imagedir'].$image_name;

		$this->ciqrcode->generate($params);

		$this->meja->setIDCafe($id_cafe);
		$this->meja->setNoMeja($this->security->xss_clean($no_meja));
		$this->meja->setQRCode($this->security->xss_clean($data_qrcode));
		$this->meja->setImageURL($this->security->xss_clean($image_name));

		$insert = $this->meja->insert_data();

		if ($insert) {
			$this->session->set_flashdata('success', 'data berhasil ditambah');
			echo json_encode(array("status" => TRUE));
		}else {
			$this->session->set_flashdata('warning', 'terjadi kesalahan');
			echo json_encode(array("status" => FALSE));
		}
	}

	function display_form_edit(){
		$id = $this->uri->segment(3);

		$this->meja->setID($id);
		$row = $this->meja->get_detail();

		$data = array(
			'id' => $row['id'],
			'no_meja' => $row['no_meja']
		);

		echo json_encode($data);
	}

	function process_edit(){
		$this->load->library('ciqrcode');
		$config['cacheable'] 	= true;
		$config['cachedir'] 	= './assests/';
		$config['errorlog'] 	= './assests/';
		$config['imagedir']		= './assets/qrcode/';
		$config['quality']		= true;
		$config['size']			= '1024';
		$config['black']		= array(224,255,255);
		$config['white']		= array(70,130,180);
		$this->ciqrcode->initialize($config);

		$id_cafe = $this->session->userdata('id_cafe');
		$nama_cafe = $this->session->userdata('nama_cafe');
		$no_meja = $this->input->post('no_meja');

		$rep_nama_cafe = str_replace(' ', '', strtolower($nama_cafe));

		$data_qrcode = $id_cafe . "_" . $no_meja . "_" . $rep_nama_cafe;

		$image_name = $data_qrcode . ".png";

		$params['data'] = $data_qrcode;
		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.$config['imagedir'].$image_name;

		$this->ciqrcode->generate($params);

		$this->meja->setID($this->security->xss_clean($this->input->post('id')));
		$this->meja->setNoMeja($this->security->xss_clean($no_meja));
		$this->meja->setQRCode($this->security->xss_clean($data_qrcode));
		$this->meja->setImageURL($this->security->xss_clean($image_name));

		$edit = $this->meja->edit_data();

		if($edit){
			$this->session->set_flashdata('success', 'data berhasil diedit');
		}else{
			$this->session->set_flashdata('warning', 'terjadi kesalahan');
		}
	}

	function process_delete(){
		$id = $_POST['id'];

		$this->meja->setID($id);
		$delete = $this->meja->delete_data();

		if($delete){
			$this->session->set_flashdata('success', 'data berhasil dihapus');
		}else{
			$this->session->set_flashdata('warning', 'terjadi kesalahan');
		}
	}

	function print_qrcode(){
		$id = $this->uri->segment(3);
		$this->meja->setID($id);
		$row = $this->meja->get_detail();

		$data['nama_cafe'] = $this->session->userdata('nama_cafe');
		$data['no_meja'] = $row['no_meja'];
		$data['qr_code'] = $row['qr_code'];
		$data['image_url'] = $row['image_url'];

		$this->load->view('print', $data);
	}

}

?>