<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div id="content-header">
	<div id="breadcrumb"> <a href="<?=site_url();?>/menus" title="Go to Home" class="tip-bottom"><i class="icon-th-list"></i> Daftar Pesanan</a></div>
	<h1>Daftar Pesanan</h1>
</div>


<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">

			<!-- Session Flash Data Pesan Error -->
			<?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success">
                <button class="close" data-dismiss="alert">x</button>
                <strong>Berhasil,</strong> <?=$this->session->flashdata('success');?>
            </div>
            <?php elseif($this->session->flashdata('warning')): ?>
            <div class="alert alert-danger">
                <button class="close" data-dismiss="alert">x</button>
                <strong>Gagal,</strong> <?=$this->session->flashdata('warning');?>
            </div>
            <?php endif; ?>
            <!-- End Session Data Pesan Error -->

			<div class="widget-box">
				<div class="widget-title">
					<span class="icon"><i class="icon-th"></i></span> 
					<h5>Data Pesanan</h5>
					
				</div>
				<div class="widget-content nopadding">
					<table class="table table-bordered data-table">
						<thead>
							<tr>
								<th width="3%">No</th>
								<th width="20%">Tanggal</th>
								<th width="10%">No Meja</th>
								<th width="55%">Total Harga</th>
								<th width="12%">Respon</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if($result > 0){
							$nomor = 1;
							foreach ($result as $key => $value) {
								$b = "";
								if($value['status'] == '0'){
									$b = "<b>";
								}else{
									$b = "";
								}
							?>
							<tr>
								<td class="center"><?=$b . $nomor;?></td>
								<td class="center"><?=$b . $value['tanggal'];?></td>
								<td class="center"><?=$b . $value['no_meja'];?></td>
								<td class="right"><?=$b . $value['totalharga'];?></td>
								<td class="center">
									<a href="<?=site_url()?>/pesanans/pesanan_detail/<?=$value['id']?>/<?=$value['no_meja']?>/<?=$value['status']?>" class="btn-primary btn-primary btn-small" target="_blank" title="Detail"><i class="icon-search icon-white"></i></a>
									<?php
									if($value['status']=='0'){
									?>
									<a href="#" onclick="finishing(<?=$value['id'];?>)" class="btn-primary btn-success btn-small" title="Finish"><i class="icon-star icon-white"></i></a>
									<?php
									}
									?>
								</td>
							</tr>
							<?php
							$nomor++;
							}
						}
						?>
						</tbody>
	      			</table>
	      		</div>
	      	</div>
	      </div>
	  </div>
</div>

<script type="text/javascript" src="<?=base_url();?>assets/js/modules/Pesanan.js"></script>