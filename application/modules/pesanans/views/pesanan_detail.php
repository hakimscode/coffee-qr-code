<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<div id="content-header">
	<div id="breadcrumb"> <a href="<?=site_url();?>/menus" title="Go to Home" class="tip-bottom"><i class="icon-th-list"></i> Detail Pesanan</a></div>
	<h1>Detail Pesanan</h1>
</div>


<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">

			<!-- Session Flash Data Pesan Error -->
			<?php if($this->session->flashdata('success')): ?>
            <div class="alert alert-success">
                <button class="close" data-dismiss="alert">x</button>
                <strong>Berhasil,</strong> <?=$this->session->flashdata('success');?>
            </div>
            <?php elseif($this->session->flashdata('warning')): ?>
            <div class="alert alert-danger">
                <button class="close" data-dismiss="alert">x</button>
                <strong>Gagal,</strong> <?=$this->session->flashdata('warning');?>
            </div>
            <?php endif; ?>
            <!-- End Session Data Pesan Error -->

            <h4>Meja no <?=$no_meja?></h4>

            <?php
            if($status != '0'){
            ?>
            	<i><h6 style="color: green;">Pesanan telah selesai</h6></i>
            <?php
            }else{
            	if($cek_status_detail){
            ?>
	            <div class="buttons">
	            	<a href="#" onclick="finishing(<?=$id_pesanan;?>)" class="btn btn-primary btn-success" title="Selesai"><i class="icon-star icon-white"></i>Selesai</a>
				</div>
			<?php
            	}else{
            		echo '<i style="color : red;">Masih ada pesanan yang belum diproses</i>';
            	}
			}
			?>

			<div class="widget-box">
				<div class="widget-title">
					<span class="icon"><i class="icon-th"></i></span> 
					<h5>Data Pesanan</h5>
					
				</div>
				<div class="widget-content nopadding">
					<table class="table table-bordered data-table">
						<thead>
							<tr>
								<th width="3%">No</th>
								<th width="30%">Pesanan</th>
								<th width="10%">@</th>
								<th width="10%">Qty</th>
								<th width="35%">Sub Harga</th>
								<th width="12%">Respon</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if($result > 0){
							$nomor = 1;
							foreach ($result as $key => $value) {
							?>
							<tr>
								<td class="center"><?=$nomor;?></td>
								<td class="center"><?=$value['menu'];?></td>
								<td class="right"><?=$value['harga'];?></td>
								<td class="center"><?=$value['qty'];?></td>
								<td class="right"><?=$value['sub_harga'];?></td>
								<td class="center">
									<?php
									if($value['status_detail'] == 0){
									?>
									<a href="#" onclick="proses_pesanan(<?=$value['id'];?>)" class="btn-primary btn-success btn-small" title="Proses">
										<i class="icon-thumbs-up icon-white"></i>
									</a>
									<?php
									}else{
										echo "Sudah Diproses";
									}
									?>
								</td>
							</tr>
							<?php
							$nomor++;
							}
						}
						?>
						</tbody>
	      			</table>
	      		</div>
	      	</div>
	      </div>
	  </div>
</div>

<script type="text/javascript" src="<?=base_url();?>assets/js/modules/Pesanan.js"></script>