<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pesanan extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	private $id;
	private $idcafe;
	private $idmeja;
	private $no_pesanan;
	private $tanggal;
	private $diskon;
	private $total_harga;
	private $status;

	//Variabel Pesanan Detail
	private $iddetail;
	private $idpesanan;
	private $idmenu;
	private $qty;
	private $diskondetail;
	private $sub_harga;
	private $status_detail;

	private $qrcode;

	function setID($id){ $this->id = $id; }
	function getID(){ return $this->id; }
	function setIdCafe($idcafe){ $this->idcafe = $idcafe; }
	function getIdCafe(){ return $this->idcafe; }
	function setIdMeja($idmeja){ $this->idmeja = $idmeja; }
	function getIdMeja(){ return $this->idmeja; }
	function setNoPesanan($no_pesanan){ $this->no_pesanan =  $no_pesanan; }
	function getNoPesanan(){ return $this->no_pesanan; }
	function setTanggal($tanggal){ $this->tanggal = $tanggal; }
	function getTanggal(){ return $this->tanggal; }
	function setDiskon($diskon){ $this->diskon = $diskon; }
	function getDiskon(){ return $this->diskon; }
	function setTotalHarga($total_harga){ $this->total_harga = $total_harga; }
	function getTotalHarga(){ return $this->total_harga; }
	function setStatus($status){ $this->status = $status; }
	function getStatus(){ return $this->status; }

	//Setter Getter Pesanan Detail
	function setIDDetail($iddetail){ $this->iddetail = $iddetail; }
	function getIDDetail(){ return $this->iddetail; }
	function setIdPesanan($idpesanan){ $this->idpesanan = $idpesanan; }
	function getIdPesanan(){ return $this->idpesanan; }
	function setIdMenu($idmenu){ $this->idmenu = $idmenu; }
	function getIdMenu(){ return $this->idmenu; }
	function setQty($qty){ $this->qty = $qty; }
	function getQty(){ return $this->qty; }
	function setDiskonDetail($diskondetail){ $this->diskondetail = $diskondetail; }
	function getDiskonDetail(){ return $this->diskondetail; }
	function setSubHarga($sub_harga){ $this->sub_harga = $sub_harga; }
	function getSubHarga(){ return $this->sub_harga; }
	function setStatusDetail($status_detail){ $this->status_detail=$status_detail; }
	function getStatusDetail(){ return $this->status_detail; }

	function setQRCode($qrcode){ $this->qrcode = $qrcode; }
	function getQRCode(){ return $this->qrcode; }


	function list_data(){
		$data = array(
			'id_cafe'	=> $this->getIdCafe()
		);
		$this->db->select('
			pesanan.id as id,
			pesanan.id_cafe as id_cafe,
			pesanan.id_meja as id_meja,
			pesanan.tanggal as tanggal,
			pesanan.total_harga as totalharga,
			pesanan.status as status,
			meja.no_meja as no_meja,
		');
		$this->db->from('pesanan');
		$this->db->join('meja', 'pesanan.id_meja = meja.id');
		$this->db->where('pesanan.id_cafe', $this->getIdCafe());
		$this->db->order_by('pesanan.updated', 'desc');
		$result = $this->db->get();

		if($result->num_rows() > 0){
			return $result->result_array();
		}else{
			return NULL;
		}
	}

	function list_data_detail(){
		$data = array(
			'id_pesanan'	=> $this->getIdPesanan()
		);
		$this->db->select('
			pesanan_detail.id as id,
			pesanan_detail.id_pesanan as id_pesanan,
			pesanan_detail.id_menu as id_menu,
			pesanan_detail.sub_harga as sub_harga,
			pesanan_detail.status as status_detail,
			menu.menu as menu,
			menu.harga as harga,
			pesanan_detail.qty as qty,
			pesanan.status as status,
			meja.no_meja as no_meja,
		');
		$this->db->from('pesanan_detail');
		$this->db->join('pesanan', 'pesanan.id = pesanan_detail.id_pesanan');
		$this->db->join('menu', 'pesanan_detail.id_menu = menu.id');
		$this->db->join('meja', 'pesanan.id_meja = meja.id');
		$this->db->where('pesanan_detail.id_pesanan', $this->getIdPesanan());
		$result = $this->db->get();

		if($result->num_rows() > 0){
			return $result->result_array();
		}else{
			return NULL;
		}
	}

	function list_pesanan(){
		$data = array(
			'id_cafe'	=> $this->getIdCafe(),
			'id_meja'	=> $this->getIdMeja()
		);
		$this->db->select('
			pesanan.id as id,
			pesanan.id_cafe as id_cafe,
			pesanan.id_meja as id_meja,
			pesanan_detail.id as id_detail,
			pesanan_detail.id_menu as id_menu,
			menu.menu as menu,
			menu.harga as harga,
			pesanan_detail.qty as qty,
			pesanan.status as status
		');
		$this->db->from('pesanan');
		$this->db->join('pesanan_detail', 'pesanan_detail.id_pesanan = pesanan.id');
		$this->db->join('menu', 'pesanan_detail.id_menu = menu.id');
		$this->db->where('pesanan.status !=', 2);
		$this->db->where('pesanan.id_cafe', $this->getIdCafe());
		$this->db->where('pesanan.id_meja', $this->getIdMeja());
		$this->db->order_by('pesanan.updated', 'desc');
		$this->db->order_by('pesanan.status', 'asc');
		$result = $this->db->get();

		if($result->num_rows() > 0){
			return $result->result_array();
		}else{
			return NULL;
		}
	}

	function insert_data(){
		$data = array(
			'id_cafe'		=> $this->getIdCafe(),
			'id_meja'		=> $this->getIdMeja(),
			'no_pesanan'	=> $this->getNoPesanan(),
			'tanggal'		=> date('Y-m-d h:i:sa'),
			'diskon'		=> $this->getDiskon(),
			'total_harga'	=> $this->getTotalHarga(),
			'status'		=> 0,
			'updated'		=> date('Y-m-d h:i:sa')
		);

		$this->db->insert('pesanan', $data);
		return $this->db->insert_id();
	}

	function insert_data_detail(){
		$data = array(
			'id_pesanan'	=> $this->getIdPesanan(),
			'id_menu'		=> $this->getIdMenu(),
			'qty'			=> $this->getQty(),
			'diskon'		=> $this->getDiskonDetail(),
			'sub_harga'		=> $this->getSubHarga()
		);

		$this->db->insert('pesanan_detail', $data);
		return $this->db->insert_id();
	}

	function get_sum_subharga(){
		$this->db->select('
			sum(sub_harga) as sum_harga
		');
		$this->db->from('pesanan_detail');
		$this->db->where('id_pesanan', $this->getIdPesanan());
		$query = $this->db->get();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			return $row['sum_harga'];
		}else{
			return NULL;
		}
	}

	function cek_pesanan_detail(){
		$this->db->select('id');
		$this->db->from('pesanan_detail');
		$this->db->where('id_pesanan', $this->getIdPesanan());
		$this->db->where('status', 0);

		$query = $this->db->get();
		if($query->num_rows() > 0){
			return FALSE;
		}else{
			return TRUE;
		}
	}

	function edit_status_detail(){
		$data=array(
			'status' => $this->getStatusDetail()
		);
		$this->db->update('pesanan_detail', $data, array('id'=>$this->getIDDetail()));
		return $this->db->affected_rows();	
	}

	function edit_total_harga(){
		$data=array(
			'total_harga' => $this->getTotalHarga()
		);
		$this->db->update('pesanan', $data, array('id'=>$this->getID()));
		return $this->db->affected_rows();
	}

	function edit_status(){
		$data=array(
			'status' => $this->getStatus()
		);
		$this->db->update('pesanan', $data, array('id'=>$this->getID()));
		return $this->db->affected_rows();	
	}

	function delete_pesanan_detail(){
		$this->db->delete('pesanan_detail', array('id'=>$this->getIDDetail(), 'status'=>0));
		return $this->db->affected_rows();
	}

	function get_id_meja(){
		$this->db->select('id, no_meja');
		$this->db->from('meja');
		$this->db->where('qr_code', $this->getQRCode());
		$query = $this->db->get();

		if($query->num_rows() > 0){
			$row = $query->row_array();
			return $row['id'];
		}else{
			return NULL;
		}
	}

}

?>