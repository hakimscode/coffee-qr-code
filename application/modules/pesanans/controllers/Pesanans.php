<?php

class Pesanans extends MY_Controller{

	function __construct(){
		parent::__construct();
		$this->check_session();
		$this->load->model('pesanan');
	}

	function index(){
		$id_cafe = $this->session->userdata('id_cafe');
		$this->pesanan->setIDCafe($id_cafe);
		$data['result'] = $this->pesanan->list_data();
		$this->load->view('layout/header');
		$this->load->view('pesanan', $data);
		$this->load->view('layout/footer');
	}

	function pesanan_detail(){
		$id_pesanan = $this->uri->segment(3);
		$no_meja = $this->uri->segment(4);
		$status = $this->uri->segment(5);
		$data['id_pesanan'] = $id_pesanan;
		$data['no_meja'] = $no_meja;
		$data['status'] = $status;
		$this->pesanan->setIDPesanan($id_pesanan);
		$data['cek_status_detail'] = $this->pesanan->cek_pesanan_detail();
		$data['result'] = $this->pesanan->list_data_detail();
		$this->load->view('layout/header');
		$this->load->view('pesanan_detail', $data);
		$this->load->view('layout/footer');
	}

	function proses_pesanan_admin(){
		$id_detail = $this->uri->segment(3);
		$this->pesanan->setIDDetail($id_detail);
		$this->pesanan->setStatusDetail('1');
		$proses = $this->pesanan->edit_status_detail();

		if($proses){
			$this->session->set_flashdata('success', 'Pesanan berhasil diproses');
			echo json_encode(array("status" => TRUE));
		}else {
			$this->session->set_flashdata('warning', 'terjadi kesalahan');
			echo json_encode(array("status" => FALSE));
		}
	}

	function finishing_admin(){
		$id_pesanan = $this->uri->segment(3);
		$this->pesanan->setID($id_pesanan);
		$this->pesanan->setStatus('1');
		$finish = $this->pesanan->edit_status();

		if($finish){
			$this->session->set_flashdata('success', 'Pesanan berhasil di closing');
			echo json_encode(array("status" => TRUE));
		}else {
			$this->session->set_flashdata('warning', 'terjadi kesalahan');
			echo json_encode(array("status" => FALSE));
		}
	}

	function cek_pesanan_yang_belum_diproses(){
		$id_pesanan = $this->uri->segment(3);

		$this->pesanan->setIDPesanan($id_pesanan);
		$cek_pesanan = $this->pesanan->cek_pesanan_detail();
		echo json_encode(array("status" => $cek_pesanan));
	}

}

?>