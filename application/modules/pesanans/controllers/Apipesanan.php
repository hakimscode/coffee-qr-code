<?php

class Apipesanan extends REST_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('pesanan');
	}

	function pesanan_post(){
		$id_cafe = $this->post('id_cafe');
		$id_meja = $this->post('id_meja');
		// $no_pesanan = $this->post('no_pesanan');
		// $diskon = $this->post('diskon');
		$total_harga = $this->post('total_harga');

		$this->db->trans_start();

		$this->pesanan->setIdCafe($id_cafe);
		$this->pesanan->setIdMeja($id_meja);
		$this->pesanan->setNoPesanan('1111');
		$this->pesanan->setDiskon('0');
		$this->pesanan->setTotalHarga($total_harga);

		$insert = $this->pesanan->insert_data();

		if($insert){
			$id_pesanan = $insert;
			$id_menu = $this->post('id_menu');
			$qty = $this->post('qty');
			// $diskon_detail = $this->post('diskon_detail');
			$sub_harga = $this->post('sub_harga');

			$this->pesanan->setIdPesanan($id_pesanan);
			$this->pesanan->setIdMenu($id_menu);
			$this->pesanan->setQty($qty);
			$this->pesanan->setDiskonDetail('0');
			$this->pesanan->setSubHarga($sub_harga);

			$insert_detail = $this->pesanan->insert_data_detail();

			if($insert_detail){
				$resp['inserted_id'] = $insert;
				$resp['success'] = 1;
				$resp['message'] = "Data berhasil ditambah";
			}else{
				$resp['inserted_id'] = '';
				$resp['success'] = 0;
				$resp['message'] = "Gagal Menyimpan Pesanan Detail";	
			}
		}else{
			$resp['inserted_id'] = '';
			$resp['success'] = 0;
			$resp['message'] = "Gagal Menyimpan Pesanan";
		}

		$this->db->trans_complete();
		echo json_encode($resp);
	}

	function add_pesanan_post(){
		$id_pesanan = $this->post('id_pesanan');
		$id_menu = $this->post('id_menu');
		$qty = $this->post('qty');
		$sub_harga = $this->post('sub_harga');

		$this->db->trans_start();

		$this->pesanan->setID($id_pesanan);
		$this->pesanan->setIdPesanan($id_pesanan);
		$this->pesanan->setIdMenu($id_menu);
		$this->pesanan->setQty($qty);
		$this->pesanan->setDiskonDetail('0');
		$this->pesanan->setSubHarga($sub_harga);

		$insert_detail = $this->pesanan->insert_data_detail();
		if($insert_detail){
			$sum_harga = $this->pesanan->get_sum_subharga();

			$this->pesanan->setTotalHarga($sum_harga);
			$edit_harga = $this->pesanan->edit_total_harga();

			if($edit_harga){
				$resp['success'] = 1;
				$resp['message'] = "Berhasil Menyimpan Data";
			}else{
				$resp['success'] = 0;
				$resp['message'] = "Gagal Menyimpan Pesanan";
			}

		}else{
			$resp['success'] = 0;
			$resp['message'] = "Gagal Menyimpan Pesanan";
		}

		$this->db->trans_complete();
		echo json_encode($resp);
	}

	function listpesanan_post(){
		$id_cafe = $this->post('id_cafe');
		$qrcode = $this->post('qrcode');

		$this->pesanan->setQRCode($qrcode);
		$id_meja = $this->pesanan->get_id_meja();

		$this->pesanan->setIdCafe($id_cafe);
		$this->pesanan->setIdMeja($id_meja);

		$resp['pesanan'] = array();

		$data = $this->pesanan->list_pesanan();
		if($data){
			foreach ($data as $key => $val) {
				$pesanan = array();
				$pesanan['id'] = $val['id'];
				$pesanan['id_cafe'] = $val['id_cafe'];
				$pesanan['id_meja'] = $val['id_meja'];
				$pesanan['id_detail'] = $val['id_detail'];
				$pesanan['id_menu'] = $val['id_menu'];
				$pesanan['menu'] = $val['menu'];
				$pesanan['harga'] = $val['harga'];
				$pesanan['qty'] = $val['qty'];

				array_push($resp['pesanan'], $pesanan);
			}
			$resp['success'] = 1;
			$resp['status'] = $val['status'];
			$resp['message'] = "Data Pesanan Ada";
		}else{	
			$resp['success'] = 0;
			$resp['status'] = null;
			$resp['message'] = "Data Pesanan Tidak Ada";
		}
		echo json_encode($resp);
	}

	function delete_pesanan_detail_post(){
		$id_pesanan = $this->post('id_pesanan');
		$id_detail = $this->post('id_detail');

		$this->db->trans_start();

		$this->pesanan->setIdPesanan($id_pesanan);
		$this->pesanan->setIDDetail($id_detail);

		$delete = $this->pesanan->delete_pesanan_detail();

		if($delete){
			$sum_harga = $this->pesanan->get_sum_subharga();

			$this->pesanan->setTotalHarga($sum_harga);
			$this->pesanan->setID($id_pesanan);
			$edit_harga = $this->pesanan->edit_total_harga();

			$resp['success'] = 1;
			$resp['message'] = "Pesanan Berhasil di Hapus";
		}else{
			$resp['success'] = 0;
			$resp['message'] = "Maaf pesanan anda sudah diproses, tidak bisa dibatalkan";
		}

		$this->db->trans_complete();

		echo json_encode($resp);
	}

	function change_status_post(){
		$id_pesanan = $this->post('id_pesanan');
		$status = $this->post('status');

		$this->pesanan->setID($id_pesanan);
		$this->pesanan->setStatus($status);

		$edit_status = $this->pesanan->edit_status();

		if($edit_status){
			$resp['success'] = 1;
			$resp['message'] = "Berhasil";
		}else{
			$resp['success'] = 0;
			$resp['message'] = "Gagal";
		}

		echo json_encode($resp);	
	}

}

?>