<?php

class Apiauth extends REST_Controller{

	function __construct($config='rest'){
		parent::__construct($config);
		$this->load->model('user');
	}

	//Menampilkan data GET
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $data = $this->db->get('user')->result();
        } else {
            $this->db->where('id', $id);
            $data = $this->db->get('user')->result();
        }
        $this->response($data, 200);
    }

	function login_post(){
		$username = $this->post('username');
		$password = md5($this->post('password'));

		$this->user->setUsername($username);
		$this->user->setPassword($password);

		$resp['login'] = array();

		$login = $this->user->login_user();
		if($login == "success"){
			$data = array(
				'id'			=> $this->user->getID(),
				'nama_lengkap'	=> $this->user->getNamaLengkap(),
				'nomor_hp'		=> $this->user->getNomorHP(),
				'email'			=> $this->user->getEmail()
			);
			// $this->response($data, 200);
			$resp['success'] = 1;
			$resp['message'] = 'Success Login';
			array_push($resp['login'], $data);
			echo json_encode($resp);
		}else{
			$resp['success'] = 0;
			$resp['message'] = 'Invalid Password';
			echo json_encode($resp);
		}
	}

	function register_post(){
		$nama_lengkap = $this->post('nama_lengkap');
		$nomor_hp = $this->post('nomor_hp');
		$email = $this->post('email');
		$username = $this->post('username');
		$password = md5($this->post('password'));

		$this->user->setNamaLengkap($nama_lengkap);
		$this->user->setNomorHP($nomor_hp);
		$this->user->setEmail($email);
		$this->user->setUsername($username);
		$this->user->setPassword($password);

		$insert = $this->user->insert_data();
		if($insert){
			$resp['success'] = 1;
			$resp['message'] = "Registrasi User Berhasil";
		}else{
			$resp['success'] = 0;
			$resp['message'] = "Gagal";
		}
		echo json_encode($resp);
	}

}

?>