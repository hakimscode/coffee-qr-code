<?php

class User extends CI_Model{

	function __construct(){
		parent::__construct();
	}

	private $id;
	private $nama_lengkap;
	private $nomor_hp;
	private $email;
	private $username;
	private $password;

	function setID($id){ $this->id = $id; }
	function getID(){ return $this->id; }
	function setNamaLengkap($nama_lengkap){ $this->nama_lengkap = $nama_lengkap; }
	function getNamaLengkap(){ return $this->nama_lengkap; }
	function setNomorHP($nomor_hp){ $this->nomor_hp = $nomor_hp; }
	function getNomorHP(){ return $this->nomor_hp; }
	function setEmail($email){ $this->email = $email; }
	function getEmail(){ return $this->email; }
	function setUsername($username){ $this->username = $username; }
	function getUsername(){ return $this->username; }
	function setPassword($password){ $this->password = $password; }
	function getPassword(){ return $this->password; }

	function login_user(){
		$param = array(
			'username' => $this->getUsername(),
			'password' => $this->getPassword()
		);

		$this->db->select('id, nama_lengkap, nomor_hp, email');
		$this->db->from('user');
		$this->db->where($param);
		$this->db->limit(1);

		$result = $this->db->get();

		if($result->num_rows() > 0){
			$row = $result->row_array();
			$this->setID($row['id']);
			$this->setNamaLengkap($row['nama_lengkap']);
			$this->setNomorHP($row['nomor_hp']);
			$this->setEmail($row['email']);
			return "success";
		}else{
			return "failed";
		}
	}

	function insert_data(){
		$data = array(
			'nama_lengkap'	=> $this->getNamaLengkap(),
			'nomor_hp'		=> $this->getNomorHP(),
			'email'			=> $this->getEmail(),
			'username'		=> $this->getUsername(),
			'password'		=> $this->getPassword(),
			'created'		=> date('Y-m-d H:i:s')
		);

		$this->db->insert('user', $data);
		return $this->db->insert_id();
	}
}

?>