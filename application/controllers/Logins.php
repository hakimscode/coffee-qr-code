<?php 

class Logins extends MY_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('login');
	}

	function index() {
		$this->load->view('login/login');
	}


	function proses_login(){
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		if($this->form_validation->run() == FALSE) {
			$this->load->view('login/login');
			echo "<script>alert('Validation False')</script>";
		} else {
			$this->login->setUsername($this->input->post('username'));
			$this->login->setPassword(md5($this->input->post('password')));

			if($this->login->process_login_admin() == "success"){
				$arr_user = array(
					'id' => $this->login->getID(),
					'nama_admin' => $this->login->getNama(),
					'id_cafe' => $this->login->getIDCafe(),
					'nama_cafe' => $this->login->getNamaCafe()
				);
				$this->session->set_userdata($arr_user);
				redirect('dashboards');
			}else{
				echo "<script>alert('Username dan Password Anda salah')</script>";
				$this->load->view('login/login');
			}
		}
	}

	function logout_admin(){
		$userid = $this->session->userdata('id');

		$this->login->setID($userid);
		$this->session->sess_destroy();

		$this->load->view('login/login');
	}

}

?>